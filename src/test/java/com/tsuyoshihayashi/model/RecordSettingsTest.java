package com.tsuyoshihayashi.model;

import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import junit.framework.TestCase;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Test for the record settings object to ensure correct initialization from JSON
 *
 * @author Alexey Donov
 */
public class RecordSettingsTest extends TestCase {
    private static final JSONParser parser = new JSONParser();
    private final @NotNull WMSLogger logger = WMSLoggerFactory.getLogger(RecordSettings.class);

    /**
     * Initialization from an empty JSON
     */
    public void testInsufficientFieldSet() {
        try {
            RecordSettings.fromJSON((JSONObject) parser.parse("{}"), "");
            fail("Arguments are missing");
        } catch (ParseException ignore) {
            fail("Must be parseable");
        } catch (IllegalArgumentException ignore) {
            // OK
        }
    }

    /**
     * Initialization from a JSON with minimal set of parameters
     */
    public void testMinimalFieldSet() {
        try {
            RecordSettings.fromJSON((JSONObject) parser.parse("{\"record_name\":\"\", \"limit\":0, \"hash\":\"hash\", \"hash2\":\"hash2\"}"), "");
        } catch (ParseException ignore) {
            fail("Must be parseable");
        } catch (IllegalArgumentException ignore) {
            fail("All arguments are there");
        }
    }

    /**
     * Test manual record start parameter
     */
    public void testManualStart() {
        try {
            val rs = RecordSettings.fromJSON((JSONObject) parser.parse("{\"record_name\":\"\", \"limit\":0, \"hash\":\"hash\", \"hash2\":\"hash2\", \"manual_start\":1}"), "");
            assertFalse(rs.isAutoRecord());
        } catch (ParseException ignore) {
            fail("Must be parseable");
        } catch (IllegalArgumentException ignore) {
            fail("All arguments are there");
        }
    }

    /**
     * Test automatic record start parameter
     */
    public void testAutoStart() {
        try {
            RecordSettings rs;
            rs = RecordSettings.fromJSON((JSONObject) parser.parse("{\"record_name\":\"\", \"limit\":0, \"hash\":\"hash\", \"hash2\":\"hash2\"}"), "");
            assertTrue("No manual_start means autostart", rs.isAutoRecord());
            rs = RecordSettings.fromJSON((JSONObject) parser.parse("{\"record_name\":\"\", \"limit\":0, \"hash\":\"hash\", \"hash2\":\"hash2\", \"manual_start\":0}"), "");
            assertTrue("manual_start == 0 means autostart", rs.isAutoRecord());
            rs = RecordSettings.fromJSON((JSONObject) parser.parse("{\"record_name\":\"\", \"limit\":0, \"hash\":\"hash\", \"hash2\":\"hash2\", \"manual_start\":\"false\"}"), "");
            assertTrue("manual_start == false means autostart", rs.isAutoRecord());
            rs = RecordSettings.fromJSON((JSONObject) parser.parse("{\"record_name\":\"\", \"limit\":0, \"hash\":\"hash\", \"hash2\":\"hash2\", \"manual_start\":\"true\"}"), "");
            assertTrue("manual_start == true means autostart", rs.isAutoRecord());

            rs = RecordSettings.fromJSON((JSONObject) parser.parse("{\"act\":null,\"publish_status\":\"reject\",\"reason\":\"\\u8cfc\\u5165\\u30e6\\u30fc\\u30b6\\u30fc\\u3067\\u306f\\u3042\\u308a\\u307e\\u305b\\u3093\\u3002\"}"), "");
            assertEquals("reject", rs.getPublishStatus());
            assertNotNull("reason is not null", rs.getReason());
            assertNull("transfer_status is null", rs.getTransferStatus());
            assertTrue("No manual_start means autostart", rs.isAutoRecord());

            JSONObject json = (JSONObject) parser.parse("{\"act\":null,\"publish_status\":\"arrow\",\"transfer_status\":\"rec.publish51.videog.jp\",\"record_name\":\"nLohTyDJuYei!20200220_170412-N-DDHHIISS-DDHHIISS.mp4\",\"limit\":1,\"manual_start\":1,\"manual_start_upload\":0,\"place\":\"https://www.videog.jp/system/api/widget/upload_api.php?hash=Ap8xGkKq&hash2=&title=&comment=&act=\",\"hash\":\"Ap8xGkKq\",\"hash2\":null,\"title\":null,\"comment\":null}");
            //System.out.print("JSON Object: " + json.toJSONString());
            rs = RecordSettings.fromJSON(json, "");
            assertEquals("arrow", rs.getPublishStatus());
            assertNull("reason is null", rs.getReason());
            assertNotNull("transfer_status is not null", rs.getTransferStatus());
            assertFalse("manual_start == 1 means no autostart", rs.isAutoRecord());


        } catch (ParseException ignore) {
            fail("Must be parseable");
        } catch (IllegalArgumentException ignore) {
            fail("All arguments are there");
        }
    }
}
