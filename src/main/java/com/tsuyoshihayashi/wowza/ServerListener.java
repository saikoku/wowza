package com.tsuyoshihayashi.wowza;

import com.wowza.wms.application.IApplication;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.server.IServer;
import com.wowza.wms.server.ServerNotifyBase;
import com.wowza.wms.vhost.IVHost;
import com.wowza.wms.vhost.VHostSingleton;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Stream;

/**
 * Object that optionally can check and remove old files from the content directory.
 * Currently is unused - this is being done by cron script.
 *
 * @author Alexey Donov
 */
public final class ServerListener extends ServerNotifyBase {
    private static final String MAX_FILE_AGE_PROPERTY_NAME = "maxFileAge";

    private final @NotNull WMSLogger logger = WMSLoggerFactory.getLogger(ServerListener.class);

    private long maxFileAge = 0;

    private final @NotNull ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);

    /**
     * Variable that stores the checking schedule
     */
    private @Nullable ScheduledFuture fileCheckFuture = null;

    /**
     * Determine if the file is considered old
     *
     * @param file File object
     * @return true if the file is old
     */
    private boolean tooOld(@NotNull File file) {
        return (System.currentTimeMillis() - file.lastModified()) / (1000 * 60 * 60 * 24) > maxFileAge;
    }

    /**
     * Log the delete action
     *
     * @param file File being deleted
     */
    private void logDeletion(@NotNull File file) {
        logger.info(String.format("File %s is too old, deleting", file.getName()));
    }

    /**
     * Check the list of files in the content directory and delete ones that are old
     */
    private void checkFiles() {

        try {
            IVHost vhost = VHostSingleton.getInstance("_defaultVHost_");
            IApplication app = vhost.getApplication("live");
            IApplicationInstance appInstance = app.getAppInstance("_definst_");

            String path = appInstance.getStreamStoragePath();
            logger.info(String.format("Check Max file age in directory %s", path));

            val root = new File(path);

            //noinspection ResultOfMethodCallIgnored
            Optional.ofNullable(root.listFiles(File::isFile))
                    .ifPresent(files -> Stream.of(files)
                            .filter(this::tooOld)
                            .peek(this::logDeletion)
                            .forEach(File::delete));
        }
        catch (Exception ex) {
            logger.error("Max file age: get directory is error", ex);
            return;
        }
    }

    /**
     * Stop checking the content directory
     *
     * @param future Schedule object
     */
    private void cancelFuture(@NotNull ScheduledFuture future) {
        future.cancel(false);
    }

    /**
     * When the server starts, schedule the checks every 1 minute
     *
     * @param server Server object
     */
    @Override
    public void onServerInit(IServer server) {

        val properties = server.getProperties();
        maxFileAge = properties.getPropertyLong(MAX_FILE_AGE_PROPERTY_NAME, 0);

        if (maxFileAge > 0) {
            logger.info(String.format("Max file age is %d day(s), starting file age checker!", maxFileAge));
            Runnable watcher = () -> {
                checkFiles();
            };
            try {
                fileCheckFuture = pool.scheduleWithFixedDelay(watcher, 0, 1, TimeUnit.HOURS);
            }
            catch (Exception ex) {
                logger.error("Max file age: scheduleWithFixedDelay is error", ex);
            }
        }
    }

    /**
     * When the server stops, remove the checking schedule
     *
     * @param server Server object
     */
    @Override
    public void onServerShutdownStart(IServer server) {
        Optional.ofNullable(fileCheckFuture).ifPresent(this::cancelFuture);
    }
}
